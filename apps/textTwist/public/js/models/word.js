module.exports.word = function (objectTemplate, getTemplate) {
  var Letter = getTemplate('./models/letter.js').Letter;

  var Word = objectTemplate.create('Word', {
    letters: {
      type: Array,
      of: Letter,
      value: []
    },
    pointValue: function () {
      return _.reduce(this.letters, function(l1, l2){
        return l1 + l2.pointValue;
      }, 0);
    },
    toString: function () {
      return _.reduce(this.letters, function(l1, l2){
        return l1 + l2.character;
      }, '');
    },
    init: function (letters) {
      this.letters = letters;
    }
  });

  return {
    Word: Word
  };
};
