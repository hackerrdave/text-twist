/*global Materialize controller*/
module.exports.board = function (objectTemplate, getTemplate) {
  var Letter      = getTemplate('./models/letter.js').Letter;
  var Word        = getTemplate('./models/word.js').Word; 

  var alphabet  = getTemplate('./lib/alphabet.js').alphabet;
  var utils     = getTemplate('./lib/utils.js').utils;

  var alphaNumbers = {
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six'
  };

  var Board = objectTemplate.create('Board', {
    startWord: {
      type: String
    },
    letters: {
      type: Array,
      of: Letter,
      value: []
    },
    selectedLetters: {
      type: Array,
      of: Letter,
      value: []
    },
    threeLetterWords: {
      type: Array,
      of: Word,
      value: []
    },
    fourLetterWords: {
      type: Array,
      of: Word,
      value: []
    },
    fiveLetterWords: {
      type: Array,
      of: Word,
      value: []
    },
    sixLetterWords: {
      type: Array,
      of: Word,
      value: []
    },
    threeLetterSolvedWords: {
      type: Array,
      of: Word,
      value: []
    },
    fourLetterSolvedWords: {
      type: Array,
      of: Word,
      value: []
    },
    fiveLetterSolvedWords: {
      type: Array,
      of: Word,
      value: []
    },
    sixLetterSolvedWords: {
      type: Array,
      of: Word,
      value: []
    },
    isReady: {
      type: Boolean,
      value: false
    },
    isActive: {
      type: Boolean,
      value: false
    },
    init: function (startWord) {
      this.isActive = true;
      var word    = startWord.toUpperCase();
      this.startWord = word;
      var letters = word.split('');

      this.buildLetters(letters);
      this.buildWordBank(startWord);
      this.shuffleLetters();
    },
    setReady: function () {
      this.isReady = true;
    },
    buildLetters: function (letters) {
      letters.forEach(this.loadLetter.bind(this));
    },
    buildWordBank: function (seedWord) {
      var setWords = function(wordData) {
        this.loadWordBank(wordData);
      }.bind(this);

      this._buildWordBank(seedWord).then(setWords);
    },
    _buildWordBank: {
      on: 'server',
      body: function (seedWord) {
        var fs = require('fs');
        var sets = fs.readFileSync(__dirname + '/../../data/sets.json');

        var setsData = JSON.parse(sets);
        var setKey = seedWord.split('').sort().join('');

        return setsData[setKey];
      }
    },
    loadWordBank: function(wordData) {
      var loadFromString = function (string) {
        var alphaNum = alphaNumbers[string.length + ''];
        this.loadWordFromString(string.toUpperCase(), alphaNum + 'LetterWords');
      }.bind(this);

      _.each(wordData.threeLetterWords, loadFromString);
      _.each(wordData.fourLetterWords, loadFromString);
      _.each(wordData.fiveLetterWords, loadFromString);
      _.each(wordData.sixLetterWords, loadFromString);
    },
    totalScore: function () {
      var solvedWords = this.threeLetterSolvedWords.concat(
        this.fourLetterSolvedWords,
        this.fiveLetterSolvedWords,
        this.sixLetterSolvedWords
      );

      return _.reduce(solvedWords, function(w1, w2){
        return w1 + w2.pointValue();
      }, 0);
    },
    displayMessage: function(message, type) {
      var typeClasses = {
        success: 'green darken-3',
        fail: 'red darken-4'
      };

      var typeClass = typeClasses[type];
      Materialize.toast(message, 2000, typeClass);
    },
    displaySuccessMessage: function (message) {
      this.displayMessage(message, 'success');
    },
    displayFailMessage: function (message) {
      this.displayMessage(message, 'fail');
    },
    displayProgressFor: function (wordLength) {
      var wordCount       = this[wordLength + 'LetterWords'].length;
      var solvedWordCount = this[wordLength + 'LetterSolvedWords'].length;

      return solvedWordCount + '/' + wordCount;
    },
    submitWord: function () {
      if (!this.isActive) { return; }

      var selectedLettersLength = this.selectedLetters.length;
      if (selectedLettersLength === 0) { return; }

      var alphaNum          = alphaNumbers[selectedLettersLength + ''];
      var wordBucket        = this[alphaNum + 'LetterWords'];
      var solvedWordBucket  = this[alphaNum + 'LetterSolvedWords'];

      var isWordMatch = function(word) {
        return word.toString() === this.selectedLetters.map(this.getCharacterFromLetter).join('');
      }.bind(this);

      var clearSelectedLetters = function () {
        this.letters = this.letters.concat(this.selectedLetters);
        this.selectedLetters = [];
      }.bind(this);

      var matchedSolvedWord = _.find(solvedWordBucket, isWordMatch);

      if (matchedSolvedWord) {
        this.displayFailMessage('Word Already Guessed!');
        clearSelectedLetters();
      }

      var matchedWord = _.find(wordBucket, isWordMatch);

      if (matchedWord) {
        this[alphaNum + 'LetterSolvedWords'].push(matchedWord);

        this.displaySuccessMessage('Correct!');
        clearSelectedLetters();
      }

      if (!matchedWord && !matchedSolvedWord) {
        this.displayFailMessage('Nope!');
        clearSelectedLetters();
      }

      controller.refresh();
    },
    shuffleLetters: function () {
      if (!this.isActive) { return; }
      if (this.letters.length === 0) { return; }

      this.letters = utils.shuffle(this.letters);

      controller.refresh();
    },
    selectLetter: function (character) {
      if (!this.isActive) { return; }
      if (this.letters.length === 0) { return; }
      var selectedLetter = _.find(this.letters, function(letter) { return letter.character === character; });
      if (!selectedLetter) { return; }

      this.selectedLetters.push(selectedLetter);
      var selectedLetterIndex = this.letters.indexOf(selectedLetter);
      this.letters.splice(selectedLetterIndex, 1);
      controller.refresh();
    },
    deselectLetter: function (character) {
      if (!this.isActive) { return; }
      if (this.selectedLetters.length === 0) { return; }

      var selectedLetter = _.find(this.selectedLetters, function(letter) { return letter.character === character; });
      this.letters.push(selectedLetter);
      var selectedLetterIndex = this.selectedLetters.indexOf(selectedLetter);
      this.selectedLetters.splice(selectedLetterIndex, 1);
      controller.refresh();
    },
    backspace: function () {
      if (!this.isActive) { return; }
      if (this.selectedLetters.length === 0) { return; }

      var letterToUnSelect = this.selectedLetters.splice(this.selectedLetters.length - 1, 1)[0];
      this.letters.push(letterToUnSelect);
      controller.refresh();
    },
    getCharacterFromLetter: function (letter) {
      return letter.character;
    },
    fetchLetterData: function (character) {
      return _.find(alphabet, function(alpha) { return alpha.character === character; });
    },
    loadLetter: function (character) {
      var alphaData = this.fetchLetterData(character);
      this.letters.push(new Letter(alphaData));
    },
    loadWordFromString: function (string, wordBucket) {
      var letters = string.split('').map(this.fetchLetterData);
      this[wordBucket].push(new Word(letters));
    },
    hasWordsForLength: function (wordLength) {
      var wordCount = this[wordLength + 'LetterWords'].length;
      return wordCount > 0;
    },
    canContinue: function () {
      return this.sixLetterSolvedWords.length > 0;
    }
  });

  return {
    Board: Board
  };
};
