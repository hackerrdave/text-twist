module.exports.letter = function (objectTemplate, _getTemplate) {

  var Letter = objectTemplate.create('Letter', {
    character: {
      type: String,
      value: ''
    },
    pointValue: {
      type: Number
    },
    frequency: {
      type: Number
    },
    init: function (alphaData) {
      this.character  = alphaData.character;
      this.pointValue = alphaData.pointValue;
      this.frequency  = alphaData.frequency;
    }
  });

  return {
    Letter: Letter
  };
};
