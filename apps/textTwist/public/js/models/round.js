module.exports.round = function (objectTemplate, getTemplate) {
  var Round = objectTemplate.create('Round', {
    startWord: {
      type: String
    },
    points: {
      type: Number
    },
    init: function (options) {
      var opts = options || {};

      this.startWord  = opts.startWord;
      this.points     = opts.points;
      this.userId     = opts.userId;
    }
  });

  return {
    Round: Round
  };
};
