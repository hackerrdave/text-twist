module.exports.user = function (objectTemplate, getTemplate) {
  var Round = getTemplate('./models/round.js').Round;

  var User = objectTemplate.create('User', {
    shortId: {
      type: String
    },
    name: {
      type: String
    },
    score: {
      type: Number
    },
    rounds: {
      type: Array,
      of: Round,
      value: []
    },
    init: function () {
      this.shortId = require('shortid').generate();
      this.score = 0;
    },
    finishedRound: function (options) {
      //startWord, points
      var opts = options || {};

      this.rounds.push(new Round({
        startWord: opts.startWord,
        userId: this.shortId,
        points: opts.points
      }));
    },
    totalScore: function () {
      return _.reduce(this.rounds, function (r1, r2) {
        return r1 + r2.points;
      }, 0);
    }
  });

  return {
    User: User
  };
};
