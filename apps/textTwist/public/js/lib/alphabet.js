module.exports.alphabet = function (_objectTemplate, _getTemplate) {

  var alphabet = [
    { character: 'A', pointValue: 1, frequency: .08167 },
    { character: 'B', pointValue: 1, frequency: .01492 },
    { character: 'C', pointValue: 1, frequency: .02782 },
    { character: 'D', pointValue: 1, frequency: .04253 },
    { character: 'E', pointValue: 1, frequency: .12702 },
    { character: 'F', pointValue: 1, frequency: .02228 },
    { character: 'G', pointValue: 1, frequency: .02105 },
    { character: 'H', pointValue: 1, frequency: .06094 },
    { character: 'I', pointValue: 1, frequency: .06966 },
    { character: 'J', pointValue: 1, frequency: .00153 },
    { character: 'K', pointValue: 1, frequency: .00772 },
    { character: 'L', pointValue: 1, frequency: .04025 },
    { character: 'M', pointValue: 1, frequency: .02406 },
    { character: 'N', pointValue: 1, frequency: .06749 },
    { character: 'O', pointValue: 1, frequency: .07507 },
    { character: 'P', pointValue: 1, frequency: .01929 },
    { character: 'Q', pointValue: 1, frequency: .00095 },
    { character: 'R', pointValue: 1, frequency: .05987 },
    { character: 'S', pointValue: 1, frequency: .06327 },
    { character: 'T', pointValue: 1, frequency: .09056 },
    { character: 'U', pointValue: 1, frequency: .02758 },
    { character: 'V', pointValue: 1, frequency: .00978 },
    { character: 'W', pointValue: 1, frequency: .02361 },
    { character: 'X', pointValue: 1, frequency: .00150 },
    { character: 'Y', pointValue: 1, frequency: .01974 },
    { character: 'Z', pointValue: 1, frequency: .00074 }
  ];

  return {
    alphabet: alphabet
  };
};
