/*eslint-disable */
//Desired Set Structure
//    {
//      "acorrt": {
//        "wordCount": 20,
//        "sixLetterWords": [
//          "carrot"
//        ],
//        "fiveLetterWords": [
//
//        ],
//        "fourLetterWords": [

//        ],
//        "threeLetterWords": [

//        ]
//      }
//    }
var fs = require('fs');

var dataDir         = 'apps/textTwist/public/data/';
var allWordsPath    = dataDir + 'all-words.csv';
var startWordsPath  = dataDir + 'start-words.csv';
var setsPath        = dataDir + 'sets.json';

var startWords  = fs.readFileSync(startWordsPath).toString().split('\n');
var allWords    = fs.readFileSync(allWordsPath).toString().split('\n');

var sets = {};
var startWord, key, wordCount;
var guessWord, guessWordKey, guessWordLetters;
var hasLetters;

  var wordCount = 0;

var alphabetLetters = 'qwertyuiopasdfghjklzxcvbnm';

var sixLetterWords    = [];
var fiveLetterWords   = [];
var fourLetterWords   = [];
var threeLetterWords  = [];

var clearMatches = function () {
  wordCount         = 0;
  sixLetterWords    = [];
  fiveLetterWords   = [];
  fourLetterWords   = [];
  threeLetterWords  = [];
}

var frequencies = function (str) {
  var frequenciesStore = {};
  for (var i = 0; i < alphabetLetters.length; i++) {
    frequenciesStore[alphabetLetters[i]] = 0;
  }

  for (var i = 0; i < str.length; i++) {
    frequenciesStore[str[i]] += 1;
  }

  return frequenciesStore;
}

var matcher = function (word) {
  var wordFrequencies = frequencies(word);
   //returns a function that matches against this word
    function matchFunc(word2) {
      var wordFrequencies2 = frequencies(word2);
      var c;
      for (var i = 0; i < alphabetLetters.length; i++) {
         c = alphabetLetters[i]
         if (wordFrequencies2[c] > wordFrequencies[c]) {
          return false
        }
         //change > to != to allow only strict anagrams
      }
      return true
    }
   return matchFunc;
 }

var findMatches = function (word, dict){
  var mf      = matcher(word);
  
  clearMatches();

  for (var i = 0; i < dict.length; i++) {
    if (mf(dict[i])) {
      wordCount += 1;
      if (dict[i].length === 6) {
        sixLetterWords.push(dict[i]);
      } else if (dict[i].length === 5) {
        fiveLetterWords.push(dict[i]);
      } else if (dict[i].length === 4) {
        fourLetterWords.push(dict[i]);
      } else if (dict[i].length === 3) {
        threeLetterWords.push(dict[i]);
      }
    }
  }

  return {
    'six': sixLetterWords,
    'five': fiveLetterWords,
    'four': fourLetterWords,
    'three': threeLetterWords
  };
}

for (var i = 0; i < startWords.length; i++) {
  startWord = startWords[i];
  key       = startWord.split('').sort().join('');

  if (sets[key]) { continue; }

  sets[key] = {
    wordCount: 0,
    sixLetterWords: [],
    fiveLetterWords: [],
    fourLetterWords: [],
    threeLetterWords: []
  };

  var matches = findMatches(startWord, allWords);

  sets[key].wordCount         = wordCount;
  sets[key].sixLetterWords    = matches.six;
  sets[key].fiveLetterWords   = matches.five;
  sets[key].fourLetterWords   = matches.four;
  sets[key].threeLetterWords  = matches.three;

  console.log('Progress: Word ' + (i + 1) + ' of ' + startWords.length);
}

// for (var j = 0; j < allWords.length; j++) {
//   guessWord         = allWords[j];
//   guessWordLetters  = guessWord.split('').sort();
//   guessWordKey      = guessWordLetters.join('');
//   totalWords        += 1;
  
//   if (sets[guessWordKey]) {
//     sets[guessWordKey].sixLetterWords.push(guessWord);
//     sets[guessWordKey].wordCount += 1;
//     savings +=1;
//   } else {
//     for (var k = 0; k < g)
//   }
// }

// 'escape'
// 'aceeps'

// 'see'
// 'ees'

var setsJson = JSON.stringify(sets, null, 2);
// console.log('Total Words Logged: ', savings);
// console.log('Total Words: ', totalWords);

fs.writeFileSync(setsPath, setsJson);
