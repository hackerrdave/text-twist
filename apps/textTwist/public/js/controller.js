/*global Mousetrap*/
module.exports.controller = function (objectTemplate, getTemplate) {
  var Board     = getTemplate('./models/board.js').Board;
  var User      = getTemplate('./models/user.js').User;

  var alphabet  = getTemplate('./lib/alphabet.js').alphabet;

  var Controller = objectTemplate.create('Controller', {
    board: {
      type: Board
    },
    user: {
      type: User
    },
    clientInit: function(_sessionExpiration) {
      this.startNewBoard();
    },
    syncSessionData: {
      on: 'server',
      body: function () {}
    },
    initializeUser: {
      on: 'server',
      body: function () {
        this.user = new User();
      }
    },
    fetchStartWord: {
      on: 'server',
      body: function() {
        var fs = require('fs');
        console.log('before words');
        var words = fs.readFileSync(__dirname + '/../data/start-words.csv').toString().split('\n');
        console.log('after words');

        //There are 15232 possible starting words, therefore randomize index between 0 & 15231
        var randomIndex = Math.floor(Math.random()*15231);

        return words[randomIndex];
      }
    },
    bindKeyboardInput: function () {
      Mousetrap.bind('space', this.board.shuffleLetters.bind(this.board));
      Mousetrap.bind('-', this.board.backspace.bind(this.board));
      Mousetrap.bind('enter', this.board.submitWord.bind(this.board));

      alphabet.forEach(function(alpha){
        Mousetrap.bind(alpha.character.toLowerCase(), this.board.selectLetter.bind(this.board, alpha.character));
      }.bind(this));
    },
    initializeBoard: function (startWord) {
      this.board = new Board(startWord);
    },
    initializeAnimations: function () {

    },
    startNewBoardAfterFail: function () {
      $('#time-up-fail').closeModal();
      this.initializeUser().then(function(){
        this.startNewBoard();
      }.bind(this));
    },
    startNewBoard: function () {
      return this.syncSessionData().then(startNewBoard.bind(this));

      function startNewBoard () {
        this.fetchStartWord().then(function(startWord){
          this.initializeBoard(startWord);
          this.board.isActive = true;
          this.bindKeyboardInput();
          this.initializeAnimations();
          this.startTimer();
        }.bind(this));
      }
    },
    startNextRound: function () {
      $('#time-up-success').closeModal();
      this.startNewBoard();
    },
    currentRound: function () {
      return this.user.rounds.length + 1;
    },
    totalScore: function () {
      return this.user.totalScore() + this.board.totalScore();
    },
    timeRemaining: {
      type: String,
      value: '02:00'
    },
    startTimer: function () {
      var duration = 120;
      var start = Date.now(), diff, minutes, seconds;
      var countdown = setInterval(timer.bind(this), 1000);

      var timeRanOut = function timeRanOut () {
        clearInterval(countdown);
        this.board.isActive = false;
        this.user.finishedRound({
          startWord: this.board.startWord,
          points: this.board.totalScore()
        });
        if (this.board.canContinue()) {
          $('#time-up-success').openModal();
        } else {
          $('#time-up-fail').openModal();
        }
      }.bind(this);

      function timer () {
        diff = duration - (((Date.now() - start) / 1000) | 0);

        minutes = (diff / 60) | 0;
        seconds = (diff % 60) | 0;

        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;

        this.timeRemaining = minutes + ':' + seconds;
        this.syncSessionData();

        if (this.timeRemaining === '00:00') { timeRanOut(); }

        if (diff <= 0) {
            // add one second so that the count down starts at the full duration
            // example 02:00 not 01:59
          start = Date.now() + 1000;
        }
      }

    },
    serverInit: function() {
      this.initializeUser();
    }
  });

  return {
    Controller: Controller
  };
};
