//Override Bindster To Prevent Data-Attributes From Being Evaluated
Bindster.prototype.isOurAttr = function(str) {
    if (hasPrefix(str, 'data-', 5)) { return false; }
    return str.substr(0, this.namespace_prefix.length + 1) === (this.namespace_prefix + ":");
};

window.onbeforeunload = function() { return 'You are about to lose your game!'; };

controller = {
    currentRound: function () { return 1; },
    totalScore: function () { return 0; },
    board: {
        hasWordsForLength: function () { return false; },
        totalScore: function() { return 0; },
        selectedLetters: [],
        letters: []
    },
    user: {
        totalScore: function () { return 0; }
    },
    timeRemaining: '2:00'
};

var bindster = Bindster.bind(controller, null, controller);
bindster.alert = console.log;

function hasPrefix(string, prefix, chars) {
    return string.substring(0, chars) === prefix;
}
