# Text Twist
Word Scrambling game build with [Amorphic](https://github.com/selsamman/amorphic) + Materialize CSS

You can view the latest live version of the app here: [Text Twist](https://text-twist-challenge.herokuapp.com/)

## Prerequisites

You will need the following items properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)

## Installation

* `git clone https://github.com/hackerrdave/text-twist.git`
* `cd text-twist`
* `npm install`

## Running / Development

To start the application on port 3001 simply run: 

`npm start`

## Running Tests

TBD
